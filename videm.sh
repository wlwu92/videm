# !/bin/bash
# Usage:
#   videm project_path

if [ $# -ne 1 ]; then
  echo "Usage: videm project_path"
  exit 1
fi

if ! [ -d $1 ]; then
  vim $1
else
  PROJECT_PATH="$(cd "$1"; pwd)"
  CONTAINER_NAME="videm_${USER}_$(basename "$PROJECT_PATH")"
  REPO=gdragon007/videm
  TAG="${REPO}:latest"
  
  if [ ! "$(docker ps -q -f name=${CONTAINER_NAME})" ]; then
    if [ "$(docker ps -aq -f status=exited -f name=${CONTAINER_NAME})" ]; then
      docker rm ${CONTAINER_NAME}
    fi
    docker run \
      -it \
      -v ${PROJECT_PATH}:/project \
      -w /project \
      --name ${CONTAINER_NAME} \
      ${TAG} \
      /bin/bash -c "vim"
  fi
fi
