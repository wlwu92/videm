# !/bin/bash
# Usage:
#   ./build.sh ./Dockerfile

DOCKERFILE=$1

CONTEXT="$(dirname "${BASH_SOURCE[0]}")"

REPO=gdragon007/videm

TAG="${REPO}:latest"

# Fail on first error.
set -e
docker build -t ${TAG} -f ${DOCKERFILE} ${CONTEXT}
echo "Built new image ${TAG}"
